from fenics import *
from dolfin import *
from mshr import *
from minio import Minio
from minio.error import ResponseError as MinioResponseError
import io
import os
import json

def do_solve(data):
    return list(data.keys())

def handle(req):
    mo_config = {
        'endpoint': os.environ['minio_endpoint'],
        'key': os.environ['minio_key'],
        'secret': os.environ['minio_secret'],
        'region': 'us-east-1',
        'bucket': os.environ['minio_bucket']
    }

    data = json.loads(req)
    res = run(data, mo_config)
    return json.dumps(res)

def run(req, mo_config):
    # Region is necessary to avoid access denied when reduced permissions ask
    # for the region for a bucket and fail
    mo = Minio(
        mo_config['endpoint'],
        access_key=mo_config['key'],
        secret_key=mo_config['secret'],
        region=mo_config['region'],
        secure=True
    )
    mo_bucket = mo_config['bucket']

    simulation_id = req['id']

    try:
        data_object = mo.get_object(mo_bucket, 'simulations/orp_simulation_{}_query.json'.format(simulation_id))
        stream_req = io.BytesIO()
        for d in data_object.stream(32*1024):
            stream_req.write(d)
    except MinioResponseError as err:
        print(err)

    data = json.loads(stream_req.getvalue().decode('utf-8'))
    res = do_solve(data)
    serial_res = bytes(json.dumps(res), encoding='utf-8')
    stream_res = io.BytesIO(serial_res)
    target = 'simulations/orp_simulation_{}_response.json'.format(simulation_id)

    try:
        mo.put_object(mo_bucket, target, stream_res, len(serial_res))
    except MinioResponseError as err:
        print(err)

    return {
        'result': res,
        'target': target
    }

if __name__ == "__main__":
    import yaml
    import sys

    try:
        inp = sys.stdin.read()
        data_inp, config_inp = inp.split('----')
        data = json.loads(data_inp)
        config = yaml.safe_load(config_inp)
    except Exception as e:
        print("For experimenting, you should pipe in the request, followed by '----', then the function's YAML config", e)
        sys.exit(1)

    env = config['functions']['orp-faas']['environment']
    mo_config = {
        'endpoint': env['minio_endpoint'],
        'key': env['minio_key'],
        'secret': env['minio_secret'],
        'region': 'us-east-1',
        'bucket': env['minio_bucket']
    }
    print(run(data, mo_config))
